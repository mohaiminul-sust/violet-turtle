import Vue from "vue";
import Router from "vue-router";
import firebase from "firebase";
import VueAnalytics from "vue-analytics";

import Home from "./views/Home.vue";
import Login from "./views/Login.vue";
import Oops from "./views/Oops.vue";
import Albums from "./views/Albums";
import AddAlbumForm from "./views/AddAlbumForm";
import Contact from "./views/Contact";
import Front from "./views/Front";
import About from "./views/About";
import Gallery from "./views/Gallery";
import Discography from "./views/Discography";

Vue.use(Router);

const router = new Router({
  mode: "history",
  base: process.env.BASE_URL,
  routes: [
    {
      path: "*",
      redirect: "/"
    },
    {
      path: "/",
      name: "Front",
      component: Front,
      meta: {
        landingpage: true
      }
    },
    {
      path: "/about",
      name: "About",
      component: About,
      meta: {
        landingpage: true
      }
    },
    {
      path: "/contact",
      name: "Contact",
      component: Contact,
      meta: {
        landingpage: true
      }
    },
    {
      path: "/gallery",
      name: "Gallery",
      component: Gallery,
      meta: {
        landingpage: true
      }
    },
    {
      path: "/discography",
      name: "Discography",
      component: Discography,
      meta: {
        landingpage: true
      }
    },
    {
      path: "/home",
      name: "home",
      component: Home,
      meta: {
        title: "Home",
        requiresAuth: true
      }
    },
    {
      path: "/login",
      name: "Login",
      component: Login,
      meta: {
        title: "Login"
      }
    },
    {
      path: "/albums",
      name: "Albums",
      component: Albums,
      meta: {
        title: "Albums",
        requiresAuth: true
      }
    },
    {
      path: "/albums/create",
      name: "AddAlbum",
      component: AddAlbumForm,
      meta: {
        title: "New Album",
        requiresAuth: true
      }
    },
    {
      path: "/oops",
      name: "Oops",
      component: Oops,
      meta: {
        title: "Oops",
        errorpage: true
      }
    }
  ]
});

router.beforeEach((to, from, next) => {
  //dynamic title display from route meta 'title'
  let titleText = "Violet Turtle";
  if (to.meta.title) {
    document.title = titleText + " - " + to.meta.title;
  } else {
    document.title = titleText;
  }
  //auth based route propagation check
  const currentUser = firebase.auth().currentUser;
  const requiresAuth = to.matched.some(record => record.meta.requiresAuth);

  if (!currentUser && requiresAuth) {
    next("login");
  } else next();
});

Vue.use(VueAnalytics, {
  id: "UA-90315220-3",
  router,
  autoTracking: {
    shouldRouterUpdate(to, from) {
      return to.path !== from.path; //only tracking when on different links
    },
    pageviewTemplate(route) {
      return {
        page: route.path,
        title: document.title,
        location: window.location.href
      };
    }
  }
});

export default router;
