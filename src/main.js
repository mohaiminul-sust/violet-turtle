import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import firebase from "firebase";

//plugins
import "./plugins/vuetify";
import "./plugins/firebase";
import "./plugins/progressbar";
import "./plugins/veevalidate";
import "./plugins/toasted";
import "./plugins/progressiveimage";
import "./plugins/vueimg";
// import "./plugins/bootstrap";

Vue.config.productionTip = false;

let app = "";
firebase.auth().onAuthStateChanged(() => {
  if (!app) {
    app = new Vue({
      router,
      render: h => h(App)
    }).$mount("#app");
  }
});
