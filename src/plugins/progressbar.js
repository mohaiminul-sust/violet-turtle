import Vue from "vue";
import VueProgressBar from "vue-progressbar";

const progressOptions = {
  color: "#e3d0f0",
  failedColor: "#FFA500",
  thickness: "10px",
  transition: {
    speed: "0.2s",
    opacity: "0.6s",
    termination: 300
  },
  autoRevert: true,
  location: "top",
  inverse: false
};

Vue.use(VueProgressBar, progressOptions);
