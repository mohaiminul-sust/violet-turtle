// import Vue from "vue";
// import VueImageLoader from "@kevindesousa/vue-image-loader";

// Vue.use(VueImageLoader);

import Vue from "vue";
import VueProgressiveImage from "vue-progressive-image";

Vue.use(VueProgressiveImage);
