# Violet Turtle
[![Netlify Status](https://api.netlify.com/api/v1/badges/117b0d94-96ae-46a9-b36c-3e8882cebecf/deploy-status)](https://app.netlify.com/sites/violetturtleband/deploys)

### Web (Vue) (Vue CLI 3 Webpack, Firebase: Auth, Database, Storage, Vuetify, Analytics, Custom handbuilt JS audio player with custom theming and anti-piracy, Progressive image loading, Animate.css, OwlCarousel, SCSS, Netlify) ###

Violet turtle’s band website (Still working on finer details). Custom layout, colors and handpicked fonts and animations. Custom built audio player (both single and with playlist).

[Deployed to netlify](https://violetturtleband.netlify.com/) | 
[Admin Panel](https://violetturtleband.netlify.com/home)

## Project setup
```
yarn install
```

### Compiles and hot-reloads for development
```
yarn serve
```

### Compiles and minifies for production
```
yarn build
```

### Lints and fixes files
```
yarn lint
```